{-|
Module: BattlePlace.WebApi.Auth
Description: Web API authentication types.
License: MIT
-}

{-# LANGUAGE DeriveGeneric #-}

module BattlePlace.WebApi.Auth
  ( ClientToken(..)
  ) where

import qualified Data.Aeson as J
import GHC.Generics(Generic)

import BattlePlace.Util
import BattlePlace.WebApi.Types

data ClientToken = ClientToken
  { clientToken_projectId :: {-# UNPACK #-} !ProjectId
  , clientToken_client :: !Client
  } deriving Generic
instance J.FromJSON ClientToken where
  parseJSON = J.genericParseJSON jsonOptions
instance J.ToJSON ClientToken where
  toJSON = J.genericToJSON jsonOptions
  toEncoding = J.genericToEncoding jsonOptions
