{-|
Module: BattlePlace.Token.Types
Description: Token types.
License: MIT
-}

{-# LANGUAGE DeriveGeneric, GeneralizedNewtypeDeriving #-}

module BattlePlace.Token.Types
  ( InternalToken(..)
  , Ticket(..)
  ) where

import qualified Data.Aeson as J
import qualified Data.Swagger as SW
import qualified Data.Text as T
import GHC.Generics(Generic)
import Servant.API

import BattlePlace.Util

-- | Internal token.
-- Token is encrypted, and contains some internal information.
newtype InternalToken a = InternalToken T.Text deriving (Generic, J.FromJSON, J.ToJSON, FromHttpApiData, ToHttpApiData)
instance SW.ToSchema (InternalToken a) where
  declareNamedSchema = SW.genericDeclareNamedSchemaNewtype swaggerSchemaOptions SW.declareSchema
instance SW.ToParamSchema (InternalToken a)

-- | Ticket.
-- Ticket doesn't contain information, it's just an ID.
newtype Ticket = Ticket T.Text deriving (Generic, J.FromJSON, J.ToJSON)
instance SW.ToSchema Ticket where
  declareNamedSchema = SW.genericDeclareNamedSchemaNewtype swaggerSchemaOptions SW.declareSchema
