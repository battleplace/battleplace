{-|
Module: BattlePlace.Skill.Glicko.Types
Description: Types for Glicko ratings.
License: MIT
-}

{-# LANGUAGE DeriveGeneric #-}

module BattlePlace.Skill.Glicko.Types
  ( Skill(..)
  , ratingFromSkill
  ) where

import qualified Data.Aeson as J
import Data.Default
import GHC.Generics(Generic)

import BattlePlace.Rating
import BattlePlace.Util

data Skill = Skill
  { skill_rating :: {-# UNPACK #-} !Double
  , skill_deviation :: {-# UNPACK #-} !Double
  , skill_volatility :: {-# UNPACK #-} !Double
  } deriving Generic

-- default skill
instance Default (Skill) where
  def = Skill
    { skill_rating = 1500
    , skill_deviation = 350
    , skill_volatility = 0.06
    }

instance J.FromJSON (Skill) where
  parseJSON = J.genericParseJSON jsonOptions
instance J.ToJSON (Skill) where
  toJSON = J.genericToJSON jsonOptions
  toEncoding = J.genericToEncoding jsonOptions

-- | Conservative rating for skill.
ratingFromSkill :: Skill -> Rating
ratingFromSkill Skill
  { skill_rating = rating
  , skill_deviation = deviation
  } = Rating $ rating - deviation * 2
