{-|
Module: BattlePlace.Rating
Description: Rating type.
License: MIT
-}

{-# LANGUAGE DeriveGeneric, GeneralizedNewtypeDeriving #-}

module BattlePlace.Rating
  ( Rating(..)
  ) where

import qualified Data.Aeson as J
import qualified Data.Swagger as SW
import GHC.Generics(Generic)

import BattlePlace.Util

-- | User rating.
newtype Rating = Rating Double deriving (Eq, Ord, Generic, J.FromJSON, J.ToJSON)
instance SW.ToSchema Rating where
  declareNamedSchema = SW.genericDeclareNamedSchemaNewtype swaggerSchemaOptions SW.declareSchema
