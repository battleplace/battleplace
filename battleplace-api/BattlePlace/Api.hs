{-|
Module: BattlePlace.Api
Description: Definitions for latest version of BattlePlace API.
License: MIT
-}

{-# OPTIONS_GHC -Wno-missing-signatures #-}

module BattlePlace.Api
  ( clientAuth
  , clientMatch
  , clientMatchStatus
  , clientMatchCancel
  , clientSessionResult
  , clientInfoStats
  , serverMatch
  , serverMatchCancel
  , serverMatchSessions
  , serverSessionResult
  , baseUrl
  , module BattlePlace.Token.Types
  , module BattlePlace.WebApi
  , module BattlePlace.WebApi.Types
  ) where

import Data.Proxy
import Servant.API((:<|>)(..))
import qualified Servant.Client as Servant

import BattlePlace.Token.Types
import BattlePlace.WebApi
import BattlePlace.WebApi.Types

api :: Servant.Client Servant.ClientM WebApi
api = Servant.client (Proxy :: Proxy WebApi)

(client :<|> server) = api

clientAuth :<|> (clientMatch :<|> clientMatchStatus :<|> clientMatchCancel) :<|> clientSessionResult :<|> clientInfoStats = client

(serverMatch :<|> serverMatchCancel :<|> serverMatchSessions) :<|> serverSessionResult = server

baseUrl :: Servant.BaseUrl
baseUrl = Servant.BaseUrl
  { Servant.baseUrlScheme = Servant.Https
  , Servant.baseUrlHost = "api.battleplace.io"
  , Servant.baseUrlPort = 443
  , Servant.baseUrlPath = "/"
  }
